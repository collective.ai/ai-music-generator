#!/bin/bash
      
for d in /src/* ; do
    echo "Building $d"
    pushd $d
    python setup.py sdist bdist_wheel

    echo "Pushing $d"
    python -m twine upload --repository gitlab dist/* --verbose
    
    echo "Cleaning dist directory"
    rm -r dist/*

    popd
done
