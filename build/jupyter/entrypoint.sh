#!/bin/env sh

pip install --no-deps -e /src/music_generator

jupyter-lab --ip=0.0.0.0 --allow-root --no-browser
