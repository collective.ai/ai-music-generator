from setuptools import find_packages, setup

setup(
    name='music-generator',
    packages=find_packages(),
    version='1.0.0',
    description='',
    author='collective.ai',
    author_email='team.collective.ai@gmail.com',
    url='https://gitlab.com/collective.ai/ai-music-generator',
    install_requires=[
        'magenta==1.3.2',
        'pyfluidsynth==1.3.0',
        'tensorflow_datasets==3.2.1'
    ],
    package_data={'': ['*.yml', '*.yaml']},
    include_package_data=True,
    classifiers=[
        'Programming Language :: Python :: 3'
    ]
)
