from tensor2tensor.utils import trainer_lib
from tensor2tensor.utils import decoding
from base64 import b64decode

from music_generator.utils import encode_midi


class MusicTransformer():
    def __init__(
            self,
            problem,
            HPARAMS_SET,
            NUM_HIDDEN_LAYERS,
            SAMPLING_METHOD,
            ALPHA,
            BEAM_SIZE,
            MODEL_NAME,
            DECODE_LENGTH,
            MODEL_PATH,
            OUTPUT_PATH):
        self.decode_length = DECODE_LENGTH
        self.model_path = MODEL_PATH
        self.output_path = OUTPUT_PATH
        self.melody_encoders = problem.get_feature_encoders()

        hparams = trainer_lib.create_hparams(hparams_set=HPARAMS_SET)
        trainer_lib.add_problem_hparams(hparams, problem)
        hparams.num_hidden_layers = NUM_HIDDEN_LAYERS
        hparams.sampling_method = SAMPLING_METHOD

        decode_hparams = decoding.decode_hparams()
        decode_hparams.alpha = ALPHA
        decode_hparams.beam_size = BEAM_SIZE

        run_config = trainer_lib.create_run_config(hparams)
        self.estimator = trainer_lib.create_estimator(
            MODEL_NAME, hparams, run_config,
            decode_hparams=decode_hparams
        )

    def generate(self, midi_string: str):
        raise NotImplementedError

    def generate_midi(self, encoded_midi: str) -> str:
        midi_string = b64decode(encoded_midi)
        output_midi = self.generate(midi_string)
        generated_midi = encode_midi(output_midi)
        return output_midi, generated_midi
