import os
import uuid
import logging

from tensor2tensor.utils import decoding
from music_generator.music_transformer import MusicTransformer
from music_generator.utils import (
    mm,
    decode,
    MelodyToPianoPerformanceProblem,
    melody_input_generator,
    get_melody_ns
)


logger = logging.getLogger(__name__)


C_HPARAMS_SET = os.getenv('C_HPARAMS_SET', 'transformer_tpu')
C_NUM_HIDDEN_LAYERS = os.getenv('C_NUM_HIDDEN_LAYERS', 16)
C_SAMPLING_METHOD = os.getenv('C_NUM_HIDDEN_LAYERS', 'random')
C_ALPHA = os.getenv('C_ALPHA', 0.0)
C_BEAM_SIZE = os.getenv('C_BEAM_SIZE', 1)
C_MODEL_NAME = os.getenv('C_MODEL_NAME', 'transformer')
C_DECODE_LENGTH = os.getenv('C_DECODE_LENGTH', 1024)
C_MODEL_PATH = os.getenv(
    'C_MODEL_PATH',
    '/resources/music_transformer_models/melody_conditioned_model_16.ckpt'
)

C_OUTPUT_PATH = os.getenv('C_OUTPUT_PATH', '/resources/midis')


class ConditionedGenerator(MusicTransformer):
    def __init__(self):
        MusicTransformer.__init__(
            self,
            problem=MelodyToPianoPerformanceProblem(),
            HPARAMS_SET=C_HPARAMS_SET,
            NUM_HIDDEN_LAYERS=C_NUM_HIDDEN_LAYERS,
            SAMPLING_METHOD=C_SAMPLING_METHOD,
            ALPHA=C_ALPHA,
            BEAM_SIZE=C_BEAM_SIZE,
            MODEL_NAME=C_MODEL_NAME,
            DECODE_LENGTH=C_DECODE_LENGTH,
            MODEL_PATH=C_MODEL_PATH,
            OUTPUT_PATH=C_OUTPUT_PATH
        )

    def generate(self, midi_string: str) -> str:

        melody_ns = get_melody_ns(midi_string)
        melody_inputs = self.melody_encoders['inputs']
        inputs = melody_inputs.encode_note_sequence(melody_ns)

        input_fn = decoding.make_input_fn_from_generator(
            melody_input_generator(inputs, C_DECODE_LENGTH))

        melody_conditioned_samples = self.estimator.predict(
            input_fn, checkpoint_path=C_MODEL_PATH)

        sample_ids = next(melody_conditioned_samples)['outputs']
        midi_filename = decode(
            sample_ids,
            encoder=self.melody_encoders['targets']
        )

        accompaniment_ns = mm.midi_file_to_note_sequence(midi_filename)

        midi_name = uuid.uuid1().hex
        output_midi = f'{C_OUTPUT_PATH}/{midi_name}.mid'
        logger.info(f'output_midi => {output_midi}')
        mm.sequence_proto_to_midi_file(accompaniment_ns, output_midi)
        return output_midi
