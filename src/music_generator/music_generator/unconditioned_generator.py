import os
import uuid
import logging

from tensor2tensor.utils import decoding
from music_generator.music_transformer import MusicTransformer
from music_generator.utils import (
    mm,
    decode,
    PianoPerformanceLanguageModelProblem,
    unconditional_input_generator,
    get_primer_ns
)


logger = logging.getLogger(__name__)


U_HPARAMS_SET = os.getenv('U_HPARAMS_SET', 'transformer_tpu')
U_NUM_HIDDEN_LAYERS = os.getenv('U_NUM_HIDDEN_LAYERS', 16)
U_SAMPLING_METHOD = os.getenv('U_NUM_HIDDEN_LAYERS', 'random')
U_ALPHA = os.getenv('U_ALPHA', 0.0)
U_BEAM_SIZE = os.getenv('U_BEAM_SIZE', 1)
U_MODEL_NAME = os.getenv('U_MODEL_NAME', 'transformer')
U_DECODE_LENGTH = os.getenv('U_DECODE_LENGTH', 1024)
U_MODEL_PATH = os.getenv(
    'U_MODEL_PATH',
    '/resources/music_transformer_models/unconditional_model_16.ckpt'
)

U_OUTPUT_PATH = os.getenv('U_OUTPUT_PATH', '/resources/midis')


class UnconditionedGenerator(MusicTransformer):
    def __init__(self):
        MusicTransformer.__init__(
            self,
            problem=PianoPerformanceLanguageModelProblem(),
            HPARAMS_SET=U_HPARAMS_SET,
            NUM_HIDDEN_LAYERS=U_NUM_HIDDEN_LAYERS,
            SAMPLING_METHOD=U_SAMPLING_METHOD,
            ALPHA=U_ALPHA,
            BEAM_SIZE=U_BEAM_SIZE,
            MODEL_NAME=U_MODEL_NAME,
            DECODE_LENGTH=U_DECODE_LENGTH,
            MODEL_PATH=U_MODEL_PATH,
            OUTPUT_PATH=U_OUTPUT_PATH
        )

    def generate(self, midi_string: str) -> str:

        primer_ns = get_primer_ns(midi_string)
        melody_targets = self.melody_encoders['targets']
        targets = melody_targets.encode_note_sequence(primer_ns)

        targets = targets[:-1]
        if len(targets) >= U_DECODE_LENGTH:
            error_msg = f'Primer has more or equal events than \
                maximum sequence length: \
                {len(targets)} >= {U_DECODE_LENGTH}; Aborting'
            raise ValueError(error_msg)

        decode_length = U_DECODE_LENGTH - len(targets)
        input_fn = decoding.make_input_fn_from_generator(
            unconditional_input_generator(targets, decode_length))

        unconditional_samples = self.estimator.predict(
            input_fn, checkpoint_path=U_MODEL_PATH)

        logger.info('Generating sample.')
        sample_ids = next(unconditional_samples)['outputs']

        logger.info('Decoding sample id')
        midi_filename = decode(sample_ids, encoder=melody_targets)
        unconditional_ns = mm.midi_file_to_note_sequence(midi_filename)
        continuation_ns = mm.concatenate_sequences(
            [primer_ns, unconditional_ns])

        logger.info('Converting note sequences to Midi file.')
        midi_name = uuid.uuid1().hex
        output_midi = f'{U_OUTPUT_PATH}/{midi_name}.mid'
        mm.sequence_proto_to_midi_file(continuation_ns, output_midi)
        return output_midi
