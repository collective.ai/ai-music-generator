import sys
import logging

from fastapi import FastAPI
from pydantic import BaseModel

from music_generator.conditioned_generator import ConditionedGenerator
from music_generator.unconditioned_generator import UnconditionedGenerator


logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logger = logging.getLogger(__name__)


generators = {
    'conditioned': ConditionedGenerator(),
    'unconditioned': UnconditionedGenerator()
}


class Midi(BaseModel):
    generator: str
    encoded_midi: str


app = FastAPI()


@app.post('/generate/')
async def generate(midi: Midi) -> dict:
    generator = generators[midi.generator]
    encoded_midi = midi.encoded_midi
    _, generated_midi = generator.generate_midi(encoded_midi)
    response = {
        'encoded_midi': generated_midi
    }

    return response
