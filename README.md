# AI Music Generator

_Implementation to use the [Google Music Tranformer Model](https://colab.research.google.com/notebooks/magenta/piano_transformer/piano_transformer.ipynb) via API_
<br/>
<br/>

Setup
-----

Install git-lfs before cloning the repo

```bash
$ git clone https://gitlab.com/collective.ai/ai-music-generator
```

This repo requires Docker 20.10 or above

If you want to run the inferences on a GPU you also need the latest version of [nvidia-container-toolkit](https://github.com/NVIDIA/nvidia-docker)

You can install nvidia-container-toolkit as follows:

```bash
$ distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
$ curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
$ curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

$ sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
$ sudo systemctl restart docker
```
<br/>

API
---

#### Build app
```bash
$ make app-build
```
#### Run app
```bash
$ make app-run
```
#### Run app (GPU)
```bash
$ make app-run-gpu
```
#### API documentation
    http://localhost:8000/docs

#### Example request
    {
        "generator": generator_type,
        "encoded_midi": input_midi
    }

_generator_type_ can be **conditioned** (improve input melody)
or **unconditioned** (melody continuation)

_input_midi_ is the input binary midi in Base64 format

#### Example response
    {
        'encoded_midi': generated_midi
    }

_encoded_midi_ is the generated binary midi in Base64
<br/>
<br/>

Optional setup
--------------

### Env setup
```bash
$ make env-setup
```

### Jupyter

#### Jupyter build
```bash
$ make jupyter-build
```
#### Jupyter run
```bash
$ make jupyter-run
```
#### Jupyter run (GPU)
```bash
$ make jupyter-run-gpu
```
