PROYECT = ai-music-generator
JUPYTER_IMAGE = $(PROYECT)-jupyter
APP_IMAGE = $(PROYECT)-app
PIP_IMAGE = $(PROYECT)-pip

env-setup:
	pipenv install --skip-lock

jupyter-build:
	docker build -t $(JUPYTER_IMAGE) -f ./build/jupyter/Dockerfile .

jupyter-run:
	docker run -it --rm \
	-v $(PWD)/notebooks:/jupyter \
	-v $(PWD)/src:/src \
	-v $(PWD)/resources:/resources \
	--network host $(JUPYTER_IMAGE)

jupyter-run-gpu:
	docker run -it --rm \
	--network host --gpus all \
	--shm-size 8G \
	-v $(PWD)/notebooks:/jupyter \
	-v $(PWD)/src:/src \
	-v $(PWD)/resources:/resources \
	$(JUPYTER_IMAGE)

app-build:
	docker build -t $(APP_IMAGE) -f ./build/app/Dockerfile .

app-run:
	docker run -it --rm \
	--network host \
	-v $(PWD)/resources:/resources $(APP_IMAGE)

app-run-gpu:
	docker run -it --rm \
	--network host --gpus all \
	--shm-size 8G \
	-v $(PWD)/resources:/resources $(APP_IMAGE)

pip-build:
	docker build -t $(PIP_IMAGE) -f ./build/pip-packages/Dockerfile .

pip-publish: pip-build
	docker run -it --rm $(PIP_IMAGE)
